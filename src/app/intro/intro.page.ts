import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// se define la variable router para saltar de una vista a otra (entre páginas).


@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  // opciones o características de los slides
  slideOpt =
  {
    // el slide inicial es el cero.
    initialSlide: 0,
    // únicamente ver un slide por vista.
    slidesPerView: 1,
    // slide centrado.
    centeredSlides: true,
    // velocidad de transición entre slide de 400 milisegundos.
    speed: 400,
    // transición automática entre slide.
    autoplay: {delay: 3000, disableOnInteraction: false}
  }

  // Slides Dinámico de "home.page.html"
  // variable "slides = [{}, {}, {}]" => es un agrelo que tiene 3 objetos ".json" en su interior (las llaves),
  slides =
  [
    // Objeto .json # 1.
    {
      // Slide 1.
      img: "assets/img/logoGitLab.png",
      textimg: "Imagen No Disponible",
      title: "Título 1",
      subtitle: "Subtítulo 1",
      icon: "arrow-forward-circle",
      description: "Descripción de contenido"  
    },

    // Objeto .json # 2.
    {
      // Slide 2.
      img: "assets/img/logoIonic.png",
      textimg: "Imagen No Disponible",
      title: "Título 2",
      subtitle: "Subtítulo 2",
      icon: "play",
      description: "Descripción de contenido"
    },
    
    // Objeto .json # 3.
    {
      // Slide 3.
      img: "assets/img/logoAngularjs.png",
      textimg: "Imagen No Disponible",
      title: "Título 3",
      subtitle: "Subtítulo 3",
      icon: "stop",
      description: "Descripción de contenido" 
    }
  ]

  // se inyecta en el constructor la varible router de tipo Router.
  constructor(private router: Router) { }
  
  // declaración de la función finish, para cerrar slide y redirigir a home
  finish()
  {
    // la variable "router" se debe definir, es decir, al inicio importar
    this.router.navigateByUrl("/home");
  }

  ngOnInit() {
  }

}
