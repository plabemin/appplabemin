import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  // opciones o características de los slides
  slideOpt =
  {
    // el slide inicial es el cero.
    initialSlide: 0,
    // únicamente ver un slide por vista.
    slidesPerView: 1,
    // slide centrado.
    centeredSlides: true,
    // velocidad de transición entre slide de 400 milisegundos.
    speed: 400,
    // transición automática entre slide.
    autoplay: {delay: 3000, disableOnInteraction: false}
  }

  constructor() {}

}
